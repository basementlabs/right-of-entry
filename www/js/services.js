angular.module('roeApp.services', [])

/**
 * A simple example service that returns some data.
 */
.factory('Visits', function() {


     testVisits = [
        
        // {id:1421038488166, location:"Kingston Foreshore", date:"23 Jun 2014", desc:"Alleged Safety Breach", eg:"visit-summary-sb", allowedEntry:true},
        // {id:1421038488167, location:"Kingston Foreshore", date:"7 Jun 2014", desc:"Request for Safety Chat", eg:"visit-summary-sg", allowedEntry:true},
        // {id:1421038488168, location:"Molongolo Residential", date:"22 May 2014", desc:"Alleged Fair-Work Breach", eg:"visit-summary-ib", disallowedEntry:true},
        // {id:1421038488169, location:"Molongolo Residential", date:"20 May 2014", desc:"Membership Chat", eg:"visit-summary-ig", unsureEntry:true},
        // {id:1421038488171, location:"Molongolo Residential", date:"19 May 2014", desc:"Request to use the bathroom", eg:"visit-summary-nr", disallowedEntry:true},
        // {id:1421038488161, location:"Kingston Foreshore", date:"2 Mar 2014", desc:"Barry's brother in-law cut sick", eg:"visit-summary-nr", disallowedEntry:true},
        // {id:1421038488162, location:"Kingston Foreshore", date:"11 Sept 2013", desc:"Scare tactics", eg:"visit-summary-nr", disallowedEntry:true}

      ];





	var store = {

		collectionName: 'visits',

		save:function( ) {
		    window.localStorage.setItem(this.collectionName, JSON.stringify(this.collection));
		},
		
		all:function() {
		    var value = window.localStorage.getItem(this.collectionName);
		    this.collection = value ? JSON.parse(value) : testVisits;
		    return this.collection;
		},
		
		insert:function(item){
			if(!this.collection){
				this.all();
			}
			this.collection.push(item);
			this.save();
			return this.collection.length -1;

		},

		update:function(item){
			if(!this.collection){
				this.all();
			}

			
			var match;
			$.each(this.collection, function(i, elem){
				console.log(elem);
				if(elem.id == item.id){
					match = elem;
				}
			});

			if (match){
				var i = this.collection.indexOf(item);
				this.collection[i] = item;
				this.save();
				return true;
			}

			return false;	
			
		},

		remove:function(id){
			if(!this.collection){
				this.all();
			}

			var match;
			$.each(this.collection, function(i, elem){
				if(elem.id == id){
					match = elem;
				}
			});

			if (match){
				this.collection.splice(this.collection.indexOf(match), 1);
				this.save();
				return true;
			}

			return false;	
			
		}
	}





	return {
		all: function(){
			return store.all();
		},
		get: function(id){

			var visits = store.all();
			
			var result;
			$.each(visits, function(i, elem){
				if(elem.id == id){
					result = elem;
				}
			});

			return result;
		},
		save: function(visit){
			//var record = this.get(visit.id);

			console.log('saving:', JSON.stringify(visit));

			
			return (store.update(visit)  || store.insert(visit));


		},
		remove:function(id){
			return store.remove(id);
		}
	};

});
