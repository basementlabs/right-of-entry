
// Common directive for Focus

angular.module('roeApp.directives', [])

	.directive('focusMe', function($timeout) {
		return {
			scope : {trigger : '@focus' },
			link : function(scope, element, attrs) {
			 	scope.$watch('trigger', function(value) {
			  		//if (value === "true") {
			   			console.log('before focus');
			   			$timeout(function() {
			   				element[0].focus();
			   				var len = $(element[0]).val().length;
			   				element[0].setSelectionRange(len, len);
			   				console.log('after after');
			   			}, 800);

			  		//}
			 	});
	   		}
	  	};

	})

	.directive('listensForStop',function($timeout){
		return {
			link:function($scope, $element){
				// $(document).bind('playbackStopped', function(){console.log('wooot')})

				$(document).bind('playbackStopped', function(e){
					console.log('received playback event',e);
					$timeout(function() {
						$scope.playing = false;
					},0);
					console.log($scope);

				});
		    }
		};
	})


	.directive('hasSave', function($timeout) {
		return {
			//scope : {trigger : '@focus' },
			link : function(scope, element, attrs) {
			 		scope.hideHomeBtn = true;
		 	}

	  	};

	})



	.directive('noSave', function($timeout) {
		return {
			//scope : {trigger : '@focus' },
			link : function(scope, element, attrs) {
			 		console.log('setting no save, ', scope.hideHomeBtn);

			 		scope.hideHomeBtn = false;
		 	}

	  	};

	})
;
