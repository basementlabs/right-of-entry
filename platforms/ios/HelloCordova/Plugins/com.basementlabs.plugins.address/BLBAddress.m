//
//  BLBAddress.m
//  HelloCordova
//
//  Created by Tim Ainge on 9/09/2014.
//
//

#import "BLBAddress.h"
#import <Cordova/CDV.h>


@implementation BLBAddress

CLGeocoder *geocoder;
CLPlacemark *placemark;


- (void) getAddress:(CDVInvokedUrlCommand *)command
{
    
    geocoder = [[CLGeocoder alloc] init];
    NSNumber* lat = [command.arguments objectAtIndex:0];
    NSNumber* lon = [command.arguments objectAtIndex:1];
    
    CLLocationDegrees longitude = (double) [lon doubleValue];
    CLLocationDegrees latitude = [lat doubleValue];
    CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    

    
    
    [geocoder reverseGeocodeLocation:currentLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       NSLog(@"placemarks: %@, errors: %@", placemarks, error);
                       
                       CDVPluginResult* pluginResult = nil;
                       if (error) {
                           pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.description];
                       } else {
                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
                           pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:placemark.addressDictionary];
                       }
                       
                       [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                       
                   }
     ];
    
}

@end
