//
//  BLBAddress.h
//  HelloCordova
//
//  Created by Tim Ainge on 9/09/2014.
//
//


#import <CoreLocation/CoreLocation.h>
#import <Cordova/CDVPlugin.h>

@interface BLBAddress : CDVPlugin

- (void)getAddress:(CDVInvokedUrlCommand*)command;

@end
