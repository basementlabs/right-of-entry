
//
//  BLSliderPrint.m
//  sliders
//
//  Created by Tim Ainge on 7/11/2013.
//
//

#import <CoreText/CoreText.h>
#import "BLBSummaryPrint.h"

@implementation BLBSummaryPrint

int pageWidth = 595;
int hMargin = 70;
int bodyWidth = 455;

int pageHeight = 842;
int yMargin = 40;
int bodyHeight = 762;


int lineHeight = 45;
float scaleFactor = 4.12;


//Define Colours
UIColor *h1Color;
UIColor *h2Color;
UIColor *h3Color;
UIColor *h4Color;
UIColor *strokeColor;


// Define Fonts
UIFont *h1Font;
UIFont *h2Font;
UIFont *h3Font;
UIFont *h4Font;

/// paragraph style
NSMutableParagraphStyle *pStyle;
NSInteger currentPage;



- (void)print:(CDVInvokedUrlCommand *)command
{
    
    CDVPluginResult* pluginResult = nil;
    NSString* json =        [command.arguments objectAtIndex:0];
    NSError* err = nil;
    currentPage = 1;
    
    
    
    
    
    //Define Colours
    h1Color = [UIColor colorWithRed:0.309804 green:0.309804 blue:0.309804 alpha:1.000000];
    h2Color = [UIColor colorWithRed:0.270588 green:0.270588 blue:0.270588 alpha:1.000000];
    h3Color = [UIColor colorWithRed:0.470588 green:0.470588 blue:0.470588 alpha:1.000000];
    h4Color = [UIColor colorWithRed:0.270588 green:0.270588 blue:0.270588 alpha:1.000000];
    strokeColor = [UIColor colorWithRed:0.670588 green:0.670588 blue:0.670588 alpha:1.000000];
    
    
    // Define Fonts
    h1Font = [UIFont fontWithName:@"HelveticaNeue" size:24.0];
    h2Font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
    h3Font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0];
    h4Font = [UIFont fontWithName:@"HelveticaNeue" size:12.0];
    
    /// Make a copy of the default paragraph style
    pStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    pStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    pStyle.alignment = NSTextAlignmentLeft;
    
    
    

    
    
    
    NSArray* args = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&err];
    
    
    NSDictionary* summary = [args objectAtIndex:0];
    
    //NSNumber* visitId = [summary valueForKey:@"id"];
    NSString* date = [summary valueForKey:@"date"];
    NSString* formattedDate = [summary valueForKey:@"formattedDate"];
    NSString* location = [summary valueForKey:@"location"];
    NSString* description = [summary valueForKey:@"desc"];
    NSString* names = [summary valueForKey:@"names"];
    NSString* visitType = [summary valueForKey:@"visitType"];
    NSString* typeCode = [summary valueForKey:@"type"];

    
    
    NSString* writtenNotice = [summary valueForKey:@"writtenNoticeSupplied"];
    NSString* hrsNotice = [summary valueForKey:@"twentyFourHoursNoticeSupplied"];
    
    
    NSString* hasFwPermitString = [[summary valueForKey:@"hasPermit"] valueForKey:@"fw"] ;
    NSString* hasWhsPermitString = [[summary valueForKey:@"hasPermit"] valueForKey:@"whs"] ;
    NSString* wasAllowedString = [summary valueForKey:@"allowedEntry"] ;
    NSString* wasDeniedString = [summary valueForKey:@"disallowedEntry"] ;
    NSString* didSignIn = [summary valueForKey:@"didSignIn"];
    NSString* inductionComplied = [summary valueForKey:@"inductionComplied"];
    NSString* signOutComplied = [summary valueForKey:@"didSignOut"];

    NSString* notes = [summary valueForKey:@"notes"];
    NSArray* audio = [summary valueForKey:@"audios"];
    NSArray* images = [summary valueForKey:@"images"];

    
    
    
    
    BOOL hasFwPermit =  [hasFwPermitString  isKindOfClass:[NSNull class]]  ? NO : [hasFwPermitString boolValue];
    BOOL hasWhsPermit = [hasWhsPermitString  isKindOfClass:[NSNull class]]  ? NO : [hasWhsPermitString boolValue];
    BOOL wasAllowed = [wasAllowedString  isKindOfClass:[NSNull class]]  ? NO : [wasAllowedString boolValue];
    BOOL wasDenied = [wasDeniedString  isKindOfClass:[NSNull class]]  ? NO : [wasDeniedString boolValue];
    BOOL hasWrittenNotcie = [writtenNotice isKindOfClass:[NSNull class]] ? NO : [writtenNotice boolValue];
    BOOL hasHrsNotcie = [hrsNotice isKindOfClass:[NSNull class]] ? NO : [hrsNotice boolValue];
//    BOOL signOutComplied = [signOutCompliedString isKindOfClass:[NSNull class]] ? NO : [signOutCompliedString boolValue];
    
    
    NSString* filename = [NSString stringWithFormat:@"%@ - %@.pdf", description, date];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filepath = [documentsDirectory stringByAppendingPathComponent:filename];
    
    
    
   
    
    
    
//    [self drawText:@"ROE - Site Visit Log" withFont:h1Font startingAt:CGPointMake(70.f, 30.f)];
    
    
    
    
    @try{
    
 

        // Create the PDF context using the default page size of 612 x 792.
        UIGraphicsBeginPDFContextToFile(filepath, CGRectZero, nil);
        
        
        //Cover page
        {
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 595, 842), nil);
            
            
            [@"Right of Entry Report" drawInRect:CGRectMake(hMargin, 40, bodyWidth, lineHeight)
                                  withAttributes:@{ NSFontAttributeName: h1Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h1Color}];
            

            draw1PxStroke ( UIGraphicsGetCurrentContext() , CGPointMake(hMargin, 80), CGPointMake(hMargin+bodyWidth, 80), strokeColor.CGColor);

            
            
            [description drawInRect:CGRectMake(hMargin, 100, bodyWidth, lineHeight)
                                  withAttributes:@{ NSFontAttributeName: h2Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h2Color}];
            
            
            if ([names isKindOfClass:[NSNull class]] || [names length] == 0) {
                names = @"not recorded";
            }
            
            
            [[NSString stringWithFormat:@"Officials Names: %@", names]  drawInRect:CGRectMake(hMargin, 130, bodyWidth, lineHeight)
                                    withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color}];
            
            
            
            
            [[location uppercaseString] drawInRect:CGRectMake(hMargin, 150, bodyWidth, lineHeight)
                                    withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color}];
            

            
            
            [[formattedDate uppercaseString] drawInRect:CGRectMake(hMargin, 170, bodyWidth, lineHeight)
              withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color}];
            

            
            draw1PxStroke ( UIGraphicsGetCurrentContext() , CGPointMake(hMargin, 210), CGPointMake(hMargin+bodyWidth, 210), strokeColor.CGColor);

            
            
            
            
    //        draw1PxStroke ( UIGraphicsGetCurrentContext() , CGPointMake(hMargin, 220), CGPointMake(hMargin+bodyWidth, 220), strokeColor.CGColor);

            
            NSString* entryString;
            
            if (wasAllowed) {
                entryString = @"Entry was allowed";
            } else if (wasDenied){
                entryString = @"Entry was Denied";
            } else {
                entryString = @"Entry was not specified";
            }
            
            
            
            
            [@"Site entry" drawInRect:CGRectMake(hMargin, 240, bodyWidth, lineHeight)
                       withAttributes:@{ NSFontAttributeName: h2Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h2Color }];
            
            
            
            [entryString drawInRect:CGRectMake(hMargin, 260, bodyWidth, lineHeight)
                     withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
            
            
            
            
            
            if ([visitType isKindOfClass:[NSNull class]] || [visitType length] == 0) {
                visitType = @"Type of visit not recorded";
            }
            
            
            [@"Reason for visit" drawInRect:CGRectMake(hMargin, 290, bodyWidth, lineHeight)
                       withAttributes:@{ NSFontAttributeName: h2Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h2Color }];
            
            
            
            [visitType drawInRect:CGRectMake(hMargin, 310, bodyWidth, lineHeight)
                     withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
            
            
            
            
            
    //        if ([visitType isKindOfClass:[NSNull class]] || [visitType length]) {
    //            visitType = @"Type of visit not recorded";
    //        }
    //        
            
            
            
            [@"Permits" drawInRect:CGRectMake(hMargin, 340, bodyWidth, lineHeight)
                       withAttributes:@{ NSFontAttributeName: h2Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h2Color }];
            
            
            if (! [hasWhsPermitString isKindOfClass:[NSNull class]]){
                
                [[NSString stringWithFormat:@"Entrants did%@ have a WHS ACT Permit", ( hasWhsPermit ? @"" : @" not") ]
                 drawInRect:CGRectMake(hMargin, 360, bodyWidth, lineHeight)
                 withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
                
            }
            
            
            if (! [hasFwPermitString isKindOfClass:[NSNull class]]){
                
                [[NSString stringWithFormat:@"Entrants did%@ have a FW ACT Permit", ( hasFwPermit ? @"" : @" not") ]
                 drawInRect:CGRectMake(hMargin, 380, bodyWidth, lineHeight)
                 withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
                
            }
            
            
            if ( [hasFwPermitString isKindOfClass:[NSNull class]] &&  [hasWhsPermitString isKindOfClass:[NSNull class]]){
                
                [ @"Entry permits note recorded" drawInRect:CGRectMake(hMargin, 360, bodyWidth, lineHeight)
                     withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
                
            }
            
            
            
            
            
            
            
            
            [@"Notice of visit" drawInRect:CGRectMake(hMargin, 410, bodyWidth, lineHeight)
                       withAttributes:@{ NSFontAttributeName: h2Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h2Color }];
            
            
            if ([writtenNotice isKindOfClass:[NSNull class]] &&  [hrsNotice isKindOfClass:[NSNull class]]) {
                
                [@"Notice of entry not recorded"
                        drawInRect:CGRectMake(hMargin, 430, bodyWidth, lineHeight)
                    withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
                
                
            }else{
            
                if([typeCode isEqualToString:@"safety.breach"] ){
                
                    
                    [[NSString stringWithFormat:@"Entrants did%@ provide written notice of a suspected safety breach", (hasWrittenNotcie ? @"" : @" not")] drawInRect:CGRectMake(hMargin, 430, bodyWidth, lineHeight)
                             withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
                } else {
                
                    [[NSString stringWithFormat:@"Entrants did%@ provide notice within 14 days and in advance of 24 hours", (hasHrsNotcie ? @"" : @" not")]
                            drawInRect:CGRectMake(hMargin, 430, bodyWidth, lineHeight)
                        withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
                }
                
            }
            
            
            
            
            [@"Visitor sign-in" drawInRect:CGRectMake(hMargin, 460, bodyWidth, lineHeight)
                            withAttributes:@{ NSFontAttributeName: h2Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h2Color }];
            
            if([didSignIn isKindOfClass:[NSNull class]]){
                [@"Compliance with instructions to sign-in not recorded" drawInRect:CGRectMake(hMargin, 480, bodyWidth, lineHeight)
                         withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
                
            }else{
            
                [[NSString stringWithFormat:@"Entrants did%@ comply with instruction to sign in", ([didSignIn boolValue] ? @"" : @" not")]
                         drawInRect:CGRectMake(hMargin, 480, bodyWidth, lineHeight)
                     withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
            
            }
            
            
            
            
            
            [@"Visitor induction" drawInRect:CGRectMake(hMargin, 510, bodyWidth, lineHeight)
                            withAttributes:@{ NSFontAttributeName: h2Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h2Color }];
            
            
            if([inductionComplied isKindOfClass:[NSNull class]]){
                [@"Compliance with induction requirements not recorded"
                            drawInRect:CGRectMake(hMargin, 530, bodyWidth, lineHeight)
                        withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
            
                
            }else{
            
                [[NSString stringWithFormat:@"Entrants did%@ comply with induction requirements", ([inductionComplied boolValue] ? @"" : @" not")]
                            drawInRect:CGRectMake(hMargin, 530, bodyWidth, lineHeight)
                         withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
            }
          
            
            
            
            
            
            [@"Site sign-out" drawInRect:CGRectMake(hMargin, 560, bodyWidth, lineHeight)
                              withAttributes:@{ NSFontAttributeName: h2Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h2Color }];
            
            
            if([signOutComplied isKindOfClass:[NSNull class]]){
                [@"Compliance with instructions to sign-out and exit site not recorded"
                 drawInRect:CGRectMake(hMargin, 580, bodyWidth, lineHeight)
                 withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
                
                
            }else{
                
                [[NSString stringWithFormat:@"Entrants did%@ comply with instructions to sign-out and exit", ([signOutComplied boolValue] ? @"" : @" not")]
                 drawInRect:CGRectMake(hMargin, 580, bodyWidth, lineHeight)
                 withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
            }

            
            
            [self drawPageNumber];
        
        } //end cover page
        
 
        
        // print notes
        {

                if (notes){
                
                CFAttributedStringRef currentText = CFAttributedStringCreate(NULL, (__bridge CFStringRef)notes, NULL);
                
                if (currentText) {
                    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(currentText);
                    if (framesetter) {
           
                        CFRange currentRange = CFRangeMake(0, 0);
                        BOOL done = NO;
                        
                        do {
                            
                            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 595, 842), nil);
                            
                            [@"Notes from Visit" drawInRect:CGRectMake(hMargin, 40, bodyWidth, lineHeight)
                                             withAttributes:@{ NSFontAttributeName: h1Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h1Color}];
                            
                            draw1PxStroke ( UIGraphicsGetCurrentContext() , CGPointMake(hMargin, 80), CGPointMake(hMargin+bodyWidth, 80), strokeColor.CGColor);
                            
                            // Render the current page and update the current range to
                            // point to the beginning of the next page.
                            currentRange = *[self updatePDFPage:(int)currentPage setTextRange:&currentRange setFramesetter:&framesetter];
                            
                            [self drawPageNumber];
                            
                            
                            // If we're at the end of the text, exit the loop.
                            if (currentRange.location == CFAttributedStringGetLength((CFAttributedStringRef)currentText))
                                done = YES;
                        } while (!done);
                        
                        
                        // Release the framewetter.
                        CFRelease(framesetter);

                        
                        
                    }
                }
            }
        }//end notes
        
        // images
        {
            if (images) {
                for (NSDictionary* imageDict in images) {
                    NSString* imgName = [imageDict valueForKey:@"name"];
                    NSString* imgDate =[imageDict valueForKey:@"prettyDate"];
                   
                    
                    
//                    UIImage* image = [UIImage imageWithContentsOfFile:imgPath];
                    NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *documentsDir  = [documentPaths objectAtIndex:0];
                    NSString  *pngPath = [documentsDir stringByAppendingPathComponent:imgName];
                    NSData *imageData = [NSData dataWithContentsOfFile:pngPath];
                    UIImage *image = [UIImage imageWithData:imageData];
                    
                    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 595, 842), nil);
                    
                    [@"Captured Images" drawInRect:CGRectMake(hMargin, 40, bodyWidth, lineHeight)
                                     withAttributes:@{ NSFontAttributeName: h1Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h1Color}];
                    
                    draw1PxStroke ( UIGraphicsGetCurrentContext() , CGPointMake(hMargin, 80), CGPointMake(hMargin+bodyWidth, 80), strokeColor.CGColor);
                    
                    
                    [ [NSString stringWithFormat:@"%@ - %@", imgDate, imgName ]
                     
                        drawInRect:CGRectMake(hMargin, 100, bodyWidth, lineHeight)
                    withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h2Color}];
                 
                    
                    float h =  image.size.width/ bodyWidth;
                    float v =  image.size.height /(bodyHeight - 180) ;
                    float scale = h < v ? v : h;
                    
                    UIImage *scaledImage =
                        [UIImage imageWithCGImage:[image CGImage]
                                            scale:(image.scale * scale)
                                      orientation:(image.imageOrientation)];
                        
                    
                    [scaledImage drawAtPoint:CGPointMake(hMargin, 140)];
                    
                    [self drawPageNumber];
                    
                    
                }
            }
            
            
            
        }//end images
        
        
        // print audio
        {
            
            if (audio){
                
                NSString* allAudio = @"";
                
                for (NSDictionary* audioDict in audio) {
                    NSString* name = [audioDict valueForKey:@"name"];
                    NSString* date = [audioDict valueForKey:@"prettyDate"];
                    
                    NSString* thisAudio = [NSString stringWithFormat:@"  - %@ recorded on %@", name, date];
                    allAudio = [allAudio stringByAppendingString: [NSString stringWithFormat: @"%@  \n \n", thisAudio ]];
                
                }
                
                CFAttributedStringRef currentText = CFAttributedStringCreate(NULL, (__bridge CFStringRef)allAudio, NULL);
                
                if (currentText) {
                    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(currentText);
                    if (framesetter) {
                        
                        CFRange currentRange = CFRangeMake(0, 0);
                        BOOL done = NO;
                        
                        do {
                            
                            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, 595, 842), nil);
                            
                            [@"Audio Recordings from Visit" drawInRect:CGRectMake(hMargin, 40, bodyWidth, lineHeight)
                                             withAttributes:@{ NSFontAttributeName: h1Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h1Color}];
                            
                            draw1PxStroke ( UIGraphicsGetCurrentContext() , CGPointMake(hMargin, 80), CGPointMake(hMargin+bodyWidth, 80), strokeColor.CGColor);
                            
                            // Render the current page and update the current range to
                            // point to the beginning of the next page.
                            currentRange = *[self updatePDFPage:(int)currentPage setTextRange:&currentRange setFramesetter:&framesetter];
                            
                            [self drawPageNumber];
                            
                            
                            // If we're at the end of the text, exit the loop.
                            if (currentRange.location == CFAttributedStringGetLength((CFAttributedStringRef)currentText))
                                done = YES;
                        } while (!done);
                        
                        
                        // Release the framewetter.
                        CFRelease(framesetter);
                        
                        
                        
                    }
                }
            }
        }//end audio
        
        
        
          // Close the PDF context and write the contents out.
        UIGraphicsEndPDFContext();
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:filepath];
        

    }
    @catch(NSException *exception){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    @finally{
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    
    
     
    
    
}

- (void) drawPageNumber{

    draw1PxStroke ( UIGraphicsGetCurrentContext() , CGPointMake(hMargin, bodyHeight), CGPointMake(hMargin+bodyWidth, bodyHeight), strokeColor.CGColor);
    
    NSString* pageNumber = [NSString stringWithFormat:@"Page %ld", (long)currentPage];
    
    currentPage++;
    
    [ pageNumber
     drawInRect:CGRectMake(bodyWidth + 35, bodyHeight+5, 40, lineHeight)
     withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
    
    
    
    [ @"This record was created using the Right of Entry App available on iTunes"
     drawInRect:CGRectMake(hMargin, bodyHeight+5, bodyWidth, lineHeight)
     withAttributes:@{ NSFontAttributeName: h3Font, NSParagraphStyleAttributeName: pStyle, NSForegroundColorAttributeName: h3Color }];
    
    
    
    

}

-(CFRange*)updatePDFPage:(int)pageNumber setTextRange:(CFRange*)pageRange setFramesetter:(CTFramesetterRef*)framesetter{
    // Get the graphics context.
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    // Put the text matrix into a known state. This ensures
    // that no old scaling factors are left in place.
    CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity);
    // Create a path object to enclose the text. Use 72 point
    // margins all around the text.
    //842-140-40-20 = 642
    // note that this is upside down x,y,w,h
    CGRect frameRect = CGRectMake(hMargin, 60, 455, 642);
    CGMutablePathRef framePath = CGPathCreateMutable();
    CGPathAddRect(framePath, NULL, frameRect);
    // Get the frame that will do the rendering.
    // The currentRange variable specifies only the starting point. The framesetter
    // lays out as much text as will fit into the frame.
    CTFrameRef frameRef = CTFramesetterCreateFrame(*framesetter, *pageRange,
                                                   framePath, NULL);
    CGPathRelease(framePath);
    // Core Text draws from the bottom-left corner up, so flip
    // the current transform prior to drawing.
    CGContextTranslateCTM(currentContext, 0, 792);
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    // Draw the frame.
    CTFrameDraw(frameRef, currentContext);

    //put it back to top left
    CGContextTranslateCTM(currentContext, 0, 792);
    CGContextScaleCTM(currentContext, 1.0, -1.0);
    
    // Update the current range based on what was drawn.
    *pageRange = CTFrameGetVisibleStringRange(frameRef);
    pageRange->location += pageRange->length;
    pageRange->length = 0;
    CFRelease(frameRef);
    return pageRange;
}




void drawSlider(NSString* title, UIFont* font, UIColor* color, NSMutableParagraphStyle* style, int x, int y, int w, int h, NSString* path, float scale ){

    [title drawInRect:CGRectMake(x, y, w, h)
       withAttributes:@{ NSFontAttributeName: font, NSParagraphStyleAttributeName: style, NSForegroundColorAttributeName: color }];
    
    UIImage *clientImg = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:path ofType:@"png"]];
    
    UIImage *scaledClientImage =
    [UIImage imageWithCGImage:[clientImg CGImage]
                        scale:(clientImg.scale * scale)
                  orientation:(clientImg.imageOrientation)];
    
    [scaledClientImage drawAtPoint:CGPointMake(x, y+20)];
    
}


void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, CGColorRef color)
{
    NSLog(@"draw px %@", @"bla" );
    
    CGContextSaveGState(context);
    CGContextSetLineCap(context, kCGLineCapSquare);
    CGContextSetStrokeColorWithColor(context, color);
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, startPoint.x + 0.5, startPoint.y + 0.5);
    CGContextAddLineToPoint(context, endPoint.x + 0.5, endPoint.y + 0.5);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
}

@end
