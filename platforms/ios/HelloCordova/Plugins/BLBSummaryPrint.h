//
//  BLSliderPrint.h
//  sliders
//
//  Created by Tim Ainge on 7/11/2013.
//
//


#import <Cordova/CDVPlugin.h>

@interface BLBSummaryPrint : CDVPlugin

    - (void) print:(CDVInvokedUrlCommand*)command;
    void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, CGColorRef color);

//- (void) draw1PxStroke:(CGContextRef) context (CGPoint) startPoint, (CGPoint) endPoint, (CGColorRef) color;


@end
