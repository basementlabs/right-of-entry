

  /* globals angular, console */
  angular.module('roeApp.controllers', [])

    .controller('masterCtrl', function($scope, $cordovaStatusbar) {
      console.log('In MASTER Controller');


      $scope.showMedia = true;
      $scope.hideHomeBtn = false;

      console.log($scope);

        //$cordovaStatusbar.overlaysWebView(true);

        // styles: Default : 0, LightContent: 1, BlackTranslucent: 2, BlackOpaque: 3
        //$cordovaStatusbar.style(1);

        // supported names: black, darkGray, lightGray, white, gray, red, green,
        // blue, cyan, yellow, magenta, orange, purple, brown
        //$cordovaStatusbar.styleColor('black');

        //$cordovaStatusbar.styleHex('#fff');

        //$cordovaStatusbar.hide();

        //$cordovaStatusbar.show();

        //var isVisible = $cordovaStatusbar.isVisible();



    })


    .controller('InfoCtrl', ['$scope', '$state', '$ionicPopup', 'Visits', '$cordovaGeolocation', '$ionicViewSwitcher', '$ionicListDelegate', '$ionicModal', function ($scope, $state, $ionicPopup, Visits, $cordovaGeolocation, $ionicViewSwitcher, $ionicListDelegate, $ionicModal) {

      //what was the entry point? info or topic
      console.log('state', $state);
      $scope.returnFrom = $state.params.returnFrom;
      $scope.returnTo = $state.params.returnTo;
      $scope.visitId = $state.params.visitId || 0;




      $scope.goBack = function(){
        $ionicViewSwitcher.nextTransition = 'back';

        if($state.current.name == $scope.returnFrom){

          if($scope.returnTo == 'visits'){
            $state.go('visits');
          }else{
            $state.go($scope.returnTo, {id:$scope.visitId});
          }

        }else{
          $state.go($state.current.up, {returnFrom:$scope.returnFrom, returnTo:$scope.returnTo, visitId:$scope.visitId});
        }

      };


      $scope.goFwd = function(toState){
        $ionicViewSwitcher.nextTransition = 'forward';

        $state.go(toState, {returnFrom:$scope.returnFrom, returnTo:$scope.returnTo, visitId:$scope.visitId});
      };


    }])

    .controller('VisitsCtrl', ['$scope', '$state', '$ionicPopup', 'Visits', '$cordovaGeolocation', '$ionicActionSheet', '$ionicListDelegate', '$ionicModal', function ($scope, $state, $ionicPopup, Visits, $cordovaGeolocation, $ionicActionSheet, $ionicListDelegate, $ionicModal) {
      'use strict';
      console.log('VisitsCtrl', $state);


        $ionicModal.fromTemplateUrl('templates/info.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.infoModal = modal;
        });


        $scope.openInfo = function() {
          $scope.infoModal.show();
        };
        $scope.closeInfo = function() {
          $scope.infoModal.hide();
        };



      $scope.prettyDate = function(shittyDate){
        return moment(shittyDate).format('D MMM YYYY');
      };


      $scope.hideHomeBtn = true;

      //$scope.visits = Visits.all();

      $scope.$watch(
        function(){
          //console.log('????????????????????????????????');
          return Visits.all();
          //console.log(Visi)

        },
        function(newVal, oldVal, scope){
          if(newVal){
            scope.visits = newVal;
            console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
          }

        },
        true);

      $scope.confirmDelete = function (visitId) {
        // show ionic actionSheet to confirm delete operation
        // show() returns a function to hide the actionSheet
        var hideSheet = $ionicActionSheet.show({
          titleText: 'Are you sure that you\'d like to delete this entry?',
          cancelText: 'Cancel',
          destructiveText: 'Delete',
          cancel: function () {
            // if the user cancel's deletion, hide the list item's delete button
            $ionicListDelegate.closeOptionButtons();
          },
          destructiveButtonClicked: function () {
            // delete expense by its id property
            Visits.remove(visitId);

            // hide the confirmation dialog
            hideSheet();
          }
        });
      };

      $scope.CreateVisit = function(){

        var newVisit = {
          id:Date.now(),
          date:Date.now(),
          hasPermit:{fw:null, whs:null},
          areCurrent:{fw:null, whs:null},
          location:"",
          desc:'',
          unsureEntry:true,
          disallowedEntry:false,
          allowedEntry:false

        };

        Visits.save(newVisit);
        $state.go('visit.evidence', {id:newVisit.id});

      };

      $scope.resume = function(visitId, path){
         $ionicListDelegate.closeOptionButtons();
        $state.go(path, {id:visitId});
      };


    }])


    .controller('SummaryCtrl', ['$scope', '$state', '$stateParams', '$ionicPopup',  '$filter', 'Visits', function ($scope, $state, $stateParams, $ionicPopup, $filter, Visits) {
      'do not use strict';

         $scope.visit = Visits.get($stateParams.id);



       console.log('summary Controller:', $scope, $state, $stateParams, $scope.visit);


       $scope.share = function(){
        console.log('sharing visit', $scope.visit);
        window.plugins.socialsharing.shareViaEmail('I recorded this report using the Right of Entry App' , 'Right of Entry', null, null, null, $scope.visit.filepath, function(){console.log('share success');}, function(){console.log('share success');});
      };

       $scope.has = function(value){
          return !( (value === '')  || (value === undefined) || (value === null) );
       };



    }])

    .controller('VisitCtrl', ['$scope', '$state', '$stateParams', '$ionicPopup', '$cordovaCamera', '$cordovaCapture', '$filter', 'Visits', '$cordovaGeolocation', '$ionicModal', function ($scope, $state, $stateParams, $ionicPopup, $cordovaCamera, $cordovaCapture, $filter, Visits, $cordovaGeolocation, $ionicModal) {
      'do not use strict';



      $scope.visit = Visits.get($stateParams.id);
      $scope.visitId = $stateParams.id;
      $scope.thisView = function(){ return $state.current.name;};

      $scope.dontHideHomeBtn = function(){
        var blacklist = ['visit.deny', 'visit.save', 'visit.notes'];

        return blacklist.indexOf($state.current.name) < 0;
      };


      // An alert dialog
     $scope.showError = function(errorTitle, errorText) {
       var alertPopup = $ionicPopup.alert({
         title: errorTitle,
         template: errorText
       });
       alertPopup.then(function(res) {
         console.log('alert shown', errorTitle, errorText);
       });
     };


      $ionicModal.fromTemplateUrl('templates/info.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.infoModal = modal;
        });


      console.log('Visit Controller:', $scope, $state, $stateParams, $scope.visit);



        $scope.showInfoDetail = function(infoUrl){
          $ionicModal.fromTemplateUrl(infoUrl, {
            scope: $scope,
            animation: 'slide-in-up'
          }).then(function(modal) {
            $scope.xxxModal = modal;
          }).then(function(){
            $scope.xxxModal.show();
          });
        };

        $scope.closeInfoDetail = function(){
          $scope.xxxModal.hide();
          $scope.xxxModal = null;
        };


        $scope.openInfo = function() {
          $scope.infoModal.show();
        };
        $scope.closeInfo = function() {
          $scope.infoModal.hide();
        };


        if($scope.visit.location === null || $scope.visit.location === ''){
          $cordovaGeolocation
          .getCurrentPosition({timeout: 20000, enableHighAccuracy: false})
          .then(function (position) {
            var lat  = position.coords.latitude;
            var long = position.coords.longitude;
            console.log(lat,long);

            window.address.get(lat,long,
              function(result){
                console.log('address',result);
                $scope.visit.location = result.Name;
                console.log('scope now', $scope);
              },
              function(err){console.log("error getting address" ,err);}

            );

          }, function(err) {
            console.log("error getting location",err);
          });
        }


      $scope.title = "Visit Log";

      console.log($scope);
      if(!$scope.visit.wiz){
        $scope.visit.wiz = {fwd:{},back:{}};
      }

      //$scope.formattedDate = moment($scope.visit.date).format('YYYY-MM-DD');
      $scope.date = new Date();
      //$scope.formattedDate = //$filter('datef')(new Date());
      $scope.recordBtn = 'button button clear icon ion-record';
      $scope.yes=true;
      $scope.no=false;


      $scope.audio= {
        recording:false,
        timerDisplay:'00:00',
        filename:'Record',
        show:true,
        timer:null,
        startTime:null,
        endTime:null,
        fullpath:null,
        localUrl:null,
        name:'Filename',


      };

      $scope.recording = false;
      $scope.timerDisplay = 'xx:xx';
      $scope.showAudio = false;




      $scope.save = function(){
         $scope.hideHomeBtn = true;

         console.log('hide home button:', $scope.hideHomeBtn);
         $scope.visit.resumePath = $state.current.name;

         if($scope.recording){
           $scope.takeAudio();
         }

         $scope.goto('visit.save');
      };



      $scope.launchInfo = function(infoPage){

        doSave(false);

        // ui-sref="recording({returnFrom:'recording', returnTo:thisView(), visitId:visitId})"
        $state.go(infoPage, {returnFrom:infoPage, returnTo:$scope.thisView(), visitId:$scope.visitId});

      };



      var doSave = function(goHome){

        $scope.visit.date = moment($scope.date);
        $scope.visit.formattedDate = moment($scope.date).format('D MMM YYYY, hh:mma');

        if($scope.visit.desc === ""){
          switch($scope.visit.type){
            case 'safety.breach':
              $scope.visit.desc = 'Safety Breach';
              break;
            case 'safety.general':
              $scope.visit.desc = 'Safety Discussions';
              break;
            case 'fair-work.breach':
              $scope.visit.desc = 'Fair Work Breach';
              break;
            case 'fair-work.general':
              $scope.visit.desc = 'Fair Work Discussions';
              break;
            default:
              $scope.visit.desc = 'Site Visit';

          }
        }


        switch($scope.visit.type){
          case 'safety.breach':
            $scope.visit.visitType = 'Safety Breach';
            break;
          case 'safety.general':
            $scope.visit.visitType = 'Safety Discussions';
            break;
          case 'fair-work.breach':
            $scope.visit.visitType = 'Fair Work Breach';
            break;
          case 'fair-work.general':
            $scope.visit.visitType = 'Fair Work Discussions';
            break;
          default:
            $scope.visit.visitType = 'Unrelated Site Visit';

        }

        if($scope.visit.desc === ""){
          $scope.visit.desc = $scope.visit.visitType;
        }




        var goFwd = function(){

          if(goHome){
            $scope.hideHomeBtn = false;
            $scope.goto('visits');
          }

        };

        var saveFilepath = function(filepath){
          $scope.visit.filepath = filepath;
          Visits.save($scope.visit);
          goFwd();
        };



        var onSuccess = function(filepath){

          // console.log("print success!", filepath, $scope.visit, Visits);
              // $scope.visit.filepath = filepath;
              // Visits.save($scope.visit);
              console.log(filepath);
              saveFilepath(filepath);

        };

        var onError = function(shouldGoHome){
              console.log('print error');
              goFwd();

        };


        Visits.save($scope.visit);

        console.log('saving and printing visit', $scope.visit);
        if(window.print){

          window.print(JSON.stringify([$scope.visit]),
            onSuccess,
            onError
          );
        }





      };


      $scope.setAssistanceRequested = function(value){
        $scope.visit.assistanceRequested = value;
                Visits.save($scope.visit);


        if(value){
          $scope.goto('visit.hsr-sign-in');
        }else{
          $scope.deny('Union officials cannot enter a site to assist a HSR unless the HSR has requested them to do so - call the police and report trespass if union officials refuse to leave.');
        }

      };


      $scope.setAllowed = function(){
        $scope.visit.unsureEntry = false;
        $scope.visit.disallowedEntry = false;
        $scope.visit.allowedEntry = true;

        doSave(function(){

        });
      };
      $scope.setDenied = function(){
        $scope.visit.unsureEntry = false;
        $scope.visit.disallowedEntry = true;
        $scope.visit.allowedEntry = false;

        doSave(function(){
          $scope.hideHomeBtn = false;
          $scope.goto('visits');
        });
      };
      $scope.setUnsure = function(){
        $scope.visit.unsureEntry = true;
        $scope.visit.disallowedEntry = false;
        $scope.visit.allowedEntry = false;

        doSave(function(){
          $scope.hideHomeBtn = false;
          $scope.goto('visits');
        });


      };


      $scope.setSignedIn = function(didSignIn){

        $scope.visit.didSignIn = didSignIn;
        var reason = $scope.visit.type;
                Visits.save($scope.visit);


        if(didSignIn){
          switch(reason){
            case 'safety.breach':
              $scope.goto('visit.induction');
            break;
            case 'safety.general':
              $scope.goto('visit.gs-induction');
            break;
            case 'safety.records':
              $scope.goto('visit.er-induction');
            break;
            case 'safety.hsr':
              $scope.goto('visit.hsr-induction');
            break;
            case 'fair-work.breach':
               $scope.goto('visit.fwb-induction');
            break;
            case 'fair-work.general':
              $scope.goto('visit.fwg-induction');
            break;

          }
        }else{
          $scope.deny('Union officials may be removed from site for failing to follow reasonable safety directions - call the police and report trespass if union officials refuse to leave.');
        }

      };

      $scope.setSignedOut = function(didSignOut){

        $scope.visit.didSignOut = didSignOut;
        var reason = $scope.visit.type;
                Visits.save($scope.visit);


        if(didSignOut){
          $scope.save();
        }else{
          $scope.deny('Call the police and report trespass if union officials refuse to leave.');
        }

      };

      $scope.setWhy = function(reason){
        $scope.visit.type = reason;
        Visits.save($scope.visit);



        switch(reason){
          case 'safety.breach':
            $scope.goto('visit.both-permits');
          break;
          case 'safety.general':
            $scope.goto('visit.gs-both-permits');
          break;
          case 'safety.records':
            $scope.goto('visit.er-both-permits');
          break;
          case 'safety.hsr':
            $scope.goto('visit.hsr-assistance');
          break;
          case 'fair-work.breach':
            $scope.goto('visit.fwb-fw-permit');
          break;
          case 'fair-work.general':
            $scope.goto('visit.fwg-fw-permit');
          break;
          default:
            $scope.deny(
              'Entry is only permitted to investigate or have discussions with workers about industrial relations or work health and safety matters - call the police and report trespass if union officials refuse to leave.',
              [
                'Note that additional rights of entry may apply under an enterprise agreement.',
                'Additional rights of entry may apply to those assisting a HSR'
              ]
            );
          break;
        }
      };


      $scope.setPermits = function(){

        Visits.save($scope.visit);

        if($scope.visit.type == 'safety.breach'){
          if( $scope.visit.hasPermit.fw && $scope.visit.hasPermit.whs){
            $scope.goto('visit.sign-in');
          }
          else{
            $scope.deny('Union officials must show both a Fair Work Act entry permit and Work Health and Safety Act entry permit before entry - call the police and report trespass if union officials refuse to comply.');
          }
        }
        else if($scope.visit.type == 'safety.general'){
          if( $scope.visit.hasPermit.fw && $scope.visit.hasPermit.whs){
            $scope.goto('visit.gs-24hrs-notice');
          }
          else{
            $scope.deny('Union officials must show both a Fair Work Act entry permit and Work Health and Safety Act entry permit before entry - call the police and report trespass if union officials refuse to comply.');
          }
        }
        else if($scope.visit.type == 'safety.records'){
          if( $scope.visit.hasPermit.fw && $scope.visit.hasPermit.whs){
            $scope.goto('visit.er-notice');
          }
          else{
            $scope.deny('Union officials must show both a Fair Work Act entry permit and Work Health and Safety Act entry permit before entry - call the police and report trespass if union officials refuse to comply.');
          }
        }
        else if($scope.visit.type == 'safety.hsr'){
          if( $scope.visit.hasPermit.fw && $scope.visit.hasPermit.whs){
            $scope.deny('TODO');
          }
          else{
            $scope.deny('TODO');
          }
        }
        else if($scope.visit.type == 'fair-work.breach'){
          if( $scope.visit.hasPermit.fw ){
            $scope.goto('visit.fwb-24hrs-notice');
          }
          else{
            $scope.deny('Union officials must show a Fair Work Act entry permit before entry - call the police and report trespass if union officials refuse to comply.');
          }
        }
        else if($scope.visit.type == 'fair-work.general'){
          if( $scope.visit.hasPermit.fw ){
            $scope.goto('visit.fwg-24hrs-notice');
          }
          else{
            $scope.deny('Union officials must show a Fair Work Act entry permit before entry - call the police and report trespass if union officials refuse to comply.');
          }
        }

      };




      $scope.deny = function(explanation, tips){
        // var denyPopup = $ionicPopup.alert({
        //     title: 'Deny Access to Site',
        //     template: explanation
        //   });
        $scope.reason = explanation;

        $scope.tips = tips;
        $scope.visit.resumePath = $state.current.name;
                Visits.save($scope.visit);


        $scope.goto('visit.deny', {msg:explanation, tips:tips});
      };


      $scope.setIdentifySafetyBreach = function(theyCould){
        $scope.visit.identifySafetyBreach = theyCould;
        Visits.save($scope.visit);

        console.log('identifySafetyBreach:', theyCould);

        if(theyCould){
          $scope.goto('visit.multiple-entrants');
        }else{
          $scope.deny('You must be able to identify a safety breach before being permitted onto a site to investigate it - call the police and report trespass if union officials refuse to leave.');
        }
      };

      $scope.setWrittenNoticeSupplied = function(supplied){
        $scope.visit.writtenNoticeSupplied = supplied;
        Visits.save($scope.visit);

        if(supplied){
          $scope.goto('visit.during-investigation');
        }else{
          $scope.goto('visit.no-notice');
        }
      };


     $scope.set24hrNoticeSupplied = function(supplied){
        $scope.visit.twentyFourHoursNoticeSupplied = supplied;
        Visits.save($scope.visit);

          switch($scope.visit.type){
            case 'fair-work.breach':
              if(supplied){
                $scope.goto('visit.fwb-sign-in');
              }else{
                $scope.deny('Union officials cannot enter to investigate alleged breaches of industrial laws unless 24 hours\' written notice has been provided in the prescribed form - call the police and report trespass if union officials refuse to leave.');
              }
            break;

            case 'fair-work.general':
              if(supplied){
                $scope.goto('visit.fwg-sign-in');
              }else{
                $scope.deny('Union officials cannot enter to hold discussions with employees about IR issues unless 24 hours\' written notice has been provided in the prescribed form  - call the police and report trespass if union officials refuse to leave.');
              }
            break;

            case 'safety.general':
              if(supplied){
                $scope.goto('visit.gs-sign-in');
              }else{
                $scope.deny('Union officials cannot enter to consult with workers about work health and safety unless 24 hours\' notice has been provided  - call the police and report trespass if union officials refuse to leave.');
              }
            break;



             case 'safety.records':
              if(supplied){
                $scope.goto('visit.er-allow-entry');
              }else{
                $scope.deny('Union officials cannot enter to inspect employee records unless 24 hours’ notice has been provided  - call the police and report trespass if union officials refuse to leave.');
              }
            break;



          }


      };


      //TODO error messages
      $scope.setInduction = function(complied){
        $scope.visit.inductionComplied = complied;
        Visits.save($scope.visit);

        if(complied){
          switch($scope.visit.type){
            case 'safety.breach':
              $scope.goto('visit.written-notice');
            break;
            case 'safety.general':
              $scope.goto('visit.gs-during-investigation');
            break;
            case 'safety.records':
              $scope.goto('visit.er-during');
            break;
            case 'safety.hsr':
              $scope.goto('visit.hsr-assist');
            break;
            case 'fair-work.breach':
               $scope.goto('visit.fwb-during-investigation');
            break;
            case 'fair-work.general':
              $scope.goto('visit.fwg-during-investigation');
            break;

          }
        }else{
          $scope.deny('Union officials may be removed from site for failing to follow reasonable safety directions - call the police and report trespass if union officials refuse to leave.');
        }


      };





      $scope.saveNotes = function(){
        $scope.hideHomeBtn = false;
        $state.go($scope.returnTo);
        $scope.visit.notes = angular.element('#notes').val() + '\n\t' + moment().format('DD/MM/YY hh:mm') +'\n\n' ;
        Visits.save($scope.visit);
        console.log($scope.visit.notes);
      };

      $scope.takeNotes = function(){
        if($state.current.name == 'visit.notes'){
          $scope.saveNotes();

        }else{
          $scope.returnTo = $state.current.name;
          $scope.hideHomeBtn = true;
          $scope.hideHomeBtn = true;
          $state.go('visit.notes');

          console.log($scope.visit.notes);


          angular.element('#notes').val( $scope.visit.notes);
          angular.element('#notes').trigger('focus');

          angular.element('#notes').val( $scope.visit.notes);
        }
      };


      $scope.openAudio = function(){
        $scope.showAudio = !$scope.showAudio;
      };

      $scope.playAudio = function(){

        if(!$scope.recording){

          console.log('play conditions', $scope.audio.filename, $scope.playing);
          //TODO - need a better way to check for a file
          if($scope.audio.filename != 'Record' && !$scope.playing){
            //grab filename
            var options = [ 0,  $scope.audio.fullpath,{limit: 3, duration: 10 } ];


            $cordovaCapture.startPlayingAudio(options).then(function(audioData) {
              console.log('started playing audio?', audioData);
              $scope.playing = true;
              console.log(navigator.device.capture);
              /*navigator.device.capture.stoppedCallback=function(m){
                $scope.playing = false;
              };*/

            });


          }else{
            console.log('stopping audio');
            $cordovaCapture.stopAudio({}).then(function(d){
              console.log('stopped with click',d);
              $scope.playing = false;
            });

          }

        }


      };

      $scope.stopAudio = function(){



      };



      $scope.takeAudio = function(){

        //console.log(this);
        //console.log($scope);

        if(!$scope.playing){

          if($scope.audio.recording){

            //TODO is this used or do we refer to scope.recording?
            $scope.audio.recording = false;
            $scope.audio.endTime = moment();

            var options = { limit: 3, duration: 10 };

            $cordovaCapture.stopAudio(options).then(function(audioData) {
              console.log(audioData);

              // Success! Audio data is here
              var i, path, len;
              for (i = 0, len = audioData.length; i < len; i += 1) {
                  $scope.audio.fullpath = audioData[i].fullPath;
                  $scope.audio.name = audioData[i].name.substring(0, audioData[i].name.length -4);
                  $scope.audio.localUrl = audioData[i].localURL;
                  $scope.audio.endTime = moment();

                  console.log('about to clear fd for timer function:', $scope.timer);
                  clearInterval($scope.timer);

                  $scope.recording = false;

                  if(!$scope.visit.audios){
                    $scope.visit.audios = [];
                  }

                  $scope.visit.audios.push(JSON.parse(JSON.stringify($scope.audio)));
                  Visits.save($scope.visit);


              }
            }, function(err) {
              // An error occured. Show a message to the user
              console.log('audio capture error code: ', err.code );
              $scope.showError('Audio Error', 'Could not save audio, try voice memo app as a fall back.');
            });


          }else{



                    var audioOptions = { limit: 3, duration: 10 };

                    $cordovaCapture.recordAudio(audioOptions).then(function(audioData) {
                      console.log('started recording', audioData);

                      // Success! Audio data is here
                      // TODO, this is super confusing because the API is geared towards multiple recorded files but we
                      //      are doing BG recording so only one at a time
                      var i, path, len;
                      for (i = 0, len = audioData.length; i < len; i += 1) {

                        //TODO is this used or do we refer to scope.recording?
                        $scope.audio.recording = !$scope.audio.recording;

                        $scope.audio.startTime = moment();
                        $scope.audio.fullpath = audioData[i].fullPath;
                        $scope.audio.name = audioData[i].name.substring(0, audioData[i].name.length -4);
                        $scope.audio.localUrl = audioData[i].localURL;
                        $scope.audio.filename = audioData[i].name;
                        $scope.audio.prettyDate = moment().format('hh:mm DD/MM/YYYY');

                        $scope.recordBtn = 'button button clear icon ion-stop';
                          $scope.recording = true;
                          $scope.playing = false;

                        $scope.timer = setInterval(function(){
                          console.log('update', $scope.audio.startTime);

                          $scope.$apply(function(){
                            $scope.audio.timerDisplay = moment().subtract($scope.audio.startTime).format('mm:ss');
                          });

                          console.log('updated', $scope.timerDisplay);
                        }, 500);

                        globalTimer = $scope.timer;
                        console.log('just set fd for timer function:', $scope.timer);




                      }
                    }, function(err) {
                      // An error occured. Show a message to the user
                      console.log('audio capture error code: ', err.code );
                      $scope.showError('Audio Error', 'Could not record audio, try voice memo app as a fall back.');
                    });


          }
        }

      };


      $scope.takePicture = function() {

           var options = { limit: 3 };

           if(!$scope.visit.images){
            $scope.visit.images = [];
           }

          $cordovaCapture.captureImage(options).then(function(imageData) {
            var i, path, len;
            for (i = 0, len = imageData.length; i < len; i += 1) {
                path = imageData[i].fullPath;
                console.log("Image data:", imageData);
                // do something interesting with the file
                $scope.visit.images.push({name:imageData[i].name, prettyDate:moment().format('DD/MM/YY hh:mm:ss') ,path:path, localPath:imageData[i].localURL,  datetime:moment()});
                Visits.save($scope.visit);
            }
          }, function(err) {
            console.log('image capture error code: ', err.code );
          });



      };


      $scope.goto = function(toState, params){
        //$state.current.fwd = toState;
        $scope.visit.wiz.fwd[$state.current.name] = toState;
        $scope.visit.wiz.back[toState] = $state.current.name;

        $scope.hideHomeBtn = false;
        $state.go(toState, params);
      };

      $scope.hasFwd = function(){
        return (typeof($scope.visit.wiz.fwd[$state.current.name]) == 'string');
      };

      $scope.hasBack = function(){
        return (typeof($scope.visit.wiz.back[$state.current.name]) == 'string');
      };

      $scope.fwd = function(){
        //TOOD validation
        //console.log('TODO - set state for this visit as completed', $state.current);
        //console.log('moving from state ', $state.current.name, ' to the next state ', $state.current.fwd, ' which is a ', typeof($state.current.fwd)  );
        $scope.hideHomeBtn = false;
        $state.go($scope.visit.wiz.fwd[$state.current.name]);
      };

      $scope.back = function(){
        //console.log('moving from state ', $state.current.name, ' to the previous state ', $state.current.back, ' which is a ', typeof($state.current.fwd)  );
        $scope.hideHomeBtn = false;
        $state.go($scope.visit.wiz.back[$state.current.name]);
      };








    }])

    .controller('AudioCtrl', ['$scope', '$state', '$stateParams', '$ionicPopup', function ($scope, $state, $stateParams, $ionicPopup) {
      'use strict';

      $scope.state = $state.current;
      $scope.visitId = $stateParams.id;
      $scope.return = $stateParams.return;

      console.log('audio controller', $scope, $stateParams, $state);


    }])


    .controller('NotesCtrl', ['$scope', '$state', '$stateParams', '$ionicPopup', function ($scope, $state, $stateParams, $ionicPopup) {
      'use strict';

      $scope.state = $state.current;
      $scope.visitId = $stateParams.id;
      $scope.return = $stateParams.return;
      $scope.hideHomeBtn = true;

      console.log('notes controller', $scope, $stateParams, $state);


    }])


    .controller('CameraCtrl', ['$scope', '$state', '$stateParams', '$ionicPopup', function ($scope, $state, $stateParams, $ionicPopup) {
      'use strict';

      $scope.state = $state.current;
      $scope.visitId = $stateParams.id;
      $scope.return = $stateParams.return;

      console.log('camera controller', $scope, $stateParams, $state);


    }])

  ;
