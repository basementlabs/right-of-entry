// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('roeApp', ['ionic',  'ui.router', 'roeApp.controllers', 'roeApp.directives', 'roeApp.services', 'ngCordova', 'ngIOS9UIWebViewPatch'])



.config( ['$compileProvider',function( $compileProvider ){
     $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https|file|blob|cdvfile):|data:image\//);
     console.log($compileProvider.imgSrcSanitizationWhitelist());
}])


.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
      console.log('there\'s a status bar here', StatusBar);
    }

    setTimeout(function() {

        if(navigator.splashscreen){
          console.log('hide later');
          navigator.splashscreen.hide();
        }
    }, 1000);

    // navigator.splashscreen.hide();

    console.log('ionic.ready');

  });
})

.run(function($rootScope){

  console.log('set logging');

  $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
    console.log('$stateChangeStart to '+toState.to+'- fired when the transition begins. toState,toParams : \n',toState, toParams);
  });
  $rootScope.$on('$stateChangeError',function(event, toState, toParams, fromState, fromParams){
    console.log('$stateChangeError - fired when an error occurs during transition.');
    console.log(arguments);
  });
  $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
    console.log('$stateChangeSuccess to '+toState.name+'- fired once the state transition is complete.');
  });

  $rootScope.$on('$viewContentLoaded',function(event){
    console.log('$viewContentLoaded - fired after dom rendered',event);
  });
  $rootScope.$on('$stateNotFound',function(event, unfoundState, fromState, fromParams){
    console.log('$stateNotFound '+unfoundState.to+'  - fired when a state cannot be found by its name.');
    console.log(unfoundState, fromState, fromParams);
  });

})


.config(function($stateProvider, $urlRouterProvider, $compileProvider) {

  console.log('configure url router');


  $stateProvider

    .state('visits', {
      url: '/visits',
      templateUrl: 'templates/visits.html',
      controller: 'VisitsCtrl'
    })


    .state('info', {
      url: '/info/:returnFrom/:returnTo/:visitId',
      templateUrl: 'templates/info.html',
      controller: 'InfoCtrl'
    })


    .state('advice', {
      url: '/info/:returnFrom/:returnTo/:visitId/advice',
      templateUrl: 'templates/info/advice.html',
      controller: 'InfoCtrl',
      up: 'info'
    })


    .state('duties', {
      url: '/info/:returnFrom/:returnTo/:visitId/duties',
      templateUrl: 'templates/info/duties.html',
      controller: 'InfoCtrl',
      up:'info'
    })



    .state('employers-obligations', {
      url: '/info/:returnFrom/:returnTo/:visitId/employers-obligations',
      templateUrl: 'templates/info/employers-obligations.html',
      controller: 'InfoCtrl',
      up:'info'
    })

    .state('union-obligations', {
      url: '/info/:returnFrom/:returnTo/:visitId/union-obligations',
      templateUrl: 'templates/info/union-obligations.html',
      controller: 'InfoCtrl',
      up:'info'
    })



    .state('permits', {
      url: '/info/:returnFrom/:returnTo/:visitId/permits',
      templateUrl: 'templates/info/permits.html',
      controller: 'InfoCtrl',
      up:'info'
    })

    .state('fw-permit', {
      url: '/info/:returnFrom/:returnTo/:visitId/fw-permit',
      templateUrl: 'templates/info/fw-permit.html',
      controller: 'InfoCtrl',
      up:'permits'
    })

    .state('whs-permit', {
      url: '/info/:returnFrom/:returnTo/:visitId/whs-permit',
      templateUrl: 'templates/info/whs-permit.html',
      controller: 'InfoCtrl',
      up:'permits'
    })


/*********************/

    .state('notice-when', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-when',
      templateUrl: 'templates/info/notice-when.html',
      controller: 'InfoCtrl',
      up:'info'
    })



    .state('notice-when-safety', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-when-safety',
      templateUrl: 'templates/info/notice-when-safety.html',
      controller: 'InfoCtrl',
      up:'notice-when'
    })

    .state('notice-when-records', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-when-records',
      templateUrl: 'templates/info/notice-when-records.html',
      controller: 'InfoCtrl',
      up:'notice-when'
    })

    .state('notice-when-hsr', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-when-hsr',
      templateUrl: 'templates/info/notice-when-hsr.html',
      controller: 'InfoCtrl',
      up:'notice-when'
    })

    .state('notice-when-ir', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-when-ir',
      templateUrl: 'templates/info/notice-when-ir.html',
      controller: 'InfoCtrl',
      up:'notice-when'
    })

    .state('notice-when-discussions', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-when-discussions',
      templateUrl: 'templates/info/notice-when-discussions.html',
      controller: 'InfoCtrl',
      up:'notice-when'
    })

    .state('notice-when-safety-discussions', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-when-safety-discussions',
      templateUrl: 'templates/info/notice-when-safety-discussions.html',
      controller: 'InfoCtrl',
      up:'notice-when'
    })

/**********************************/




    .state('entry-when', {
      url: '/info/:returnFrom/:returnTo/:visitId/entry-when',
      templateUrl: 'templates/info/entry-when.html',
      controller: 'InfoCtrl',
      up:'info'
    })



    .state('entry-when-safety', {
      url: '/info/:returnFrom/:returnTo/:visitId/entry-when-safety',
      templateUrl: 'templates/info/entry-when-safety.html',
      controller: 'InfoCtrl',
      up:'entry-when'
    })

    .state('entry-when-records', {
      url: '/info/:returnFrom/:returnTo/:visitId/entry-when-records',
      templateUrl: 'templates/info/entry-when-records.html',
      controller: 'InfoCtrl',
      up:'entry-when'
    })

    .state('entry-when-hsr', {
      url: '/info/:returnFrom/:returnTo/:visitId/entry-when-hsr',
      templateUrl: 'templates/info/entry-when-hsr.html',
      controller: 'InfoCtrl',
      up:'entry-when'
    })

    .state('entry-when-ir', {
      url: '/info/:returnFrom/:returnTo/:visitId/entry-when-ir',
      templateUrl: 'templates/info/entry-when-ir.html',
      controller: 'InfoCtrl',
      up:'entry-when'
    })

    .state('entry-when-ir-discussions', {
      url: '/info/:returnFrom/:returnTo/:visitId/entry-when-ir-discussions',
      templateUrl: 'templates/info/entry-when-ir-discussions.html',
      controller: 'InfoCtrl',
      up:'entry-when'
    })

    .state('entry-when-safety-discussions', {
      url: '/info/:returnFrom/:returnTo/:visitId/entry-when-safety-discussions',
      templateUrl: 'templates/info/entry-when-safety-discussions.html',
      controller: 'InfoCtrl',
      up:'entry-when'
    })

/**********************************/

    .state('notice-content-safety', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-content-safety',
      templateUrl: 'templates/info/notice-content-safety.html',
      controller: 'InfoCtrl',
      up:'notice-content'
    })

    .state('notice-content-records', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-content-records',
      templateUrl: 'templates/info/notice-content-records.html',
      controller: 'InfoCtrl',
      up:'notice-content'
    })


    .state('notice-content-safety-discussions', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-content-safety-discussions',
      templateUrl: 'templates/info/notice-content-safety-discussions.html',
      controller: 'InfoCtrl',
      up:'notice-content'
    })


    .state('notice-content-worksafe-template', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-content-worksafe-template',
      templateUrl: 'templates/info/notice-content-worksafe-template.html',
      controller: 'InfoCtrl',
      up:'notice-content'
    })

    .state('notice-content-ir', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-content-ir',
      templateUrl: 'templates/info/notice-content-ir.html',
      controller: 'InfoCtrl',
      up:'notice-content'
    })

    .state('notice-content-ir-discussions', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-content-ir-discussions',
      templateUrl: 'templates/info/notice-content-ir-discussions.html',
      controller: 'InfoCtrl',
      up:'notice-content'
    })


    .state('notice-content', {
      url: '/info/:returnFrom/:returnTo/:visitId/notice-content',
      templateUrl: 'templates/info/notice-content.html',
      controller: 'InfoCtrl',
      up:'info'
    })



/********************************/


    .state('where', {
      url: '/info/:returnFrom/:returnTo/:visitId/where',
      templateUrl: 'templates/info/where.html',
      controller: 'InfoCtrl',
      up:'info'
    })


    .state('safety-walks', {
      url: '/info/:returnFrom/:returnTo/:visitId/safety-walks',
      templateUrl: 'templates/info/safety-walks.html',
      controller: 'InfoCtrl',
      up:'info'
    })


    .state('shut-down', {
      url: '/info/:returnFrom/:returnTo/:visitId/shut-down',
      templateUrl: 'templates/info/shut-down.html',
      controller: 'InfoCtrl',
      up:'info'
    })

    .state('written-notice', {
      url: '/info/:returnFrom/:returnTo/:visitId/written-notice',
      templateUrl: 'templates/info/written-notice.html',
      controller: 'InfoCtrl',
      up:'info'
    })


/*************  UNION POWERS *********************/

    .state('powers', {
      url: '/info/:returnFrom/:returnTo/:visitId/powers',
      templateUrl: 'templates/info/powers.html',
      controller: 'InfoCtrl',
      up:'info'
    })

    .state('powers-safety-breach', {
      url: '/info/:returnFrom/:returnTo/:visitId/powers-safety-breach',
      templateUrl: 'templates/info/powers-safety-breach.html',
      controller: 'InfoCtrl',
      up:'powers'
    })

    .state('powers-safety-records', {
      url: '/info/:returnFrom/:returnTo/:visitId/powers-safety-records',
      templateUrl: 'templates/info/powers-safety-records.html',
      controller: 'InfoCtrl',
      up:'powers'
    })


    .state('powers-safety-discussions', {
      url: '/info/:returnFrom/:returnTo/:visitId/powers-safety-discussions',
      templateUrl: 'templates/info/powers-safety-discussions.html',
      controller: 'InfoCtrl',
      up:'powers'
    })



    .state('powers-ir-breach', {
      url: '/info/:returnFrom/:returnTo/:visitId/powers-ir-breach',
      templateUrl: 'templates/info/powers-ir-breach.html',
      controller: 'InfoCtrl',
      up:'powers'
    })

    .state('powers-ir-discussions', {
      url: '/info/:returnFrom/:returnTo/:visitId/powers-ir-discussions',
      templateUrl: 'templates/info/powers-ir-discussions.html',
      controller: 'InfoCtrl',
      up:'powers'
    })



/********************************/


    .state('recording', {
      url: '/info/:returnFrom/:returnTo/:visitId/recording',
      templateUrl: 'templates/info/recording.html',
      controller: 'InfoCtrl',
      up:'info'
    })





    .state('visit',{
      url:'/visits/:id',
      templateUrl:'templates/visit.html',
      controller: 'VisitCtrl'
    })

    .state('visit.notes',{
      url:'/notes',
      views:{
        'step':{
          templateUrl:'templates/note-tray.html',
          //controller: 'VisitCtrl'
        }
      }
    })


    .state('summary', {
      url: '/summary/:id',
      templateUrl: 'templates/summary.html',
      controller:'SummaryCtrl'
    })

    .state('visit.save', {
      url: '/save',
      views:{
        'step':{
          templateUrl: 'templates/visit-desc.html',
        }
      },
      //fwd:'visit.both-permits',
      //8back:'visit.desc'
      leftButton:'none'
    })

    .state('visit.deny', {
      url: '/deny',
      views:{
        'step':{
          templateUrl: 'templates/visit-deny.html',
        }
      },
      //fwd:'visit.both-permits',
      //8back:'visit.desc'
      leftButton:'none'
    })

    .state('visit.evidence', {
      url: '/evidence',
      views:{
        'step':{
          templateUrl: 'templates/visit-evidence.html',
        }
      },
      //fwd:'visit.both-permits',
      back:'visit.desc'
    })

    .state('visit.why', {
      url: '/why',
      views:{
        'step':{
          templateUrl: 'templates/visit-why.html',
        }
      },
      //fwd:'visit.both-permits',
      back:'visit.desc'
    })

    .state('visit.both-permits', {
      url: '/permits',
      views:{
        'step':{
          templateUrl: 'templates/safety/visit-both-permits.html',
        }
      },
      back:'visit.why',
      //fwd:'visit.both-current'
    })


    .state('visit.sign-in', {
      url: '/sign-in',
      views:{
        'step':{
          templateUrl: 'templates/safety/visit-sign-in.html',
        }
      },
      back:'visit.both-permits',
      //fwd:'visit.both-current'
    })


    .state('visit.induction', {
      url: '/induction',
      views:{
        'step':{
          templateUrl: 'templates/safety/visit-induction.html',
        }
      },
      back:'visit.sign-in',
      //fwd:'visit.both-current'
    })




    .state('visit.written-notice', {
      url: '/written-notice',
      views:{
        'step':{
          templateUrl: 'templates/safety/visit-written-notice.html',
        }
      },
      back:'visit.induction',
      //fwd:'visit.allow-entry'
    })

    .state('visit.no-notice', {
      url: '/no-notice',
      views:{
        'step':{
          templateUrl: 'templates/safety/visit-no-notice.html',
        }
      },
      //
      //back:'visit.written-notice'
    })

    // .state('visit.allow-entry', {
    //   url: '/allow-entry',
    //   views:{
    //     'step':{
    //       templateUrl: 'templates/safety/visit-allow-entry.html',
    //     }
    //   },
    //   //
    //   back:'visit.written-notice'
    // })

    .state('visit.sign-out', {
      url: '/sign-out',
      views:{
        'step':{
          templateUrl: 'templates/safety/visit-sign-out.html',
        }
      },
      //

    })

    .state('visit.during-investigation', {
      url: '/during-investigation',
      views:{
        'step':{
          templateUrl: 'templates/safety/visit-during-investigation.html',
        }
      },
      //
      back:'visit.written-notice'
    })

/****         INSPECT EMPLOYEE RECORDS     *****/



    .state('visit.er-both-permits', {
      url: '/er/both-permits',
      views:{
        'step':{
          templateUrl: 'templates/records/permits.html',
        }
      },
      back:'visit.why',
    })

    .state('visit.er-notice', {
      url: '/er/notice',
      views:{
        'step':{
          templateUrl: 'templates/records/notice.html',
        }
      },
      back:'visit.er-both-permits',
    })

    .state('visit.er-allow-entry', {
      url: '/er/allow-entry',
      views:{
        'step':{
          templateUrl: 'templates/records/allow-entry.html',
        }
      },
      back:'visit.er-notice',
    })

    .state('visit.er-induction', {
      url: '/er/induction',
      views:{
        'step':{
          templateUrl: 'templates/records/visit-induction.html',
        }
      },
      back:'visit.er-allow-entry',
      //fwd:'visit.both-current'
    })



    .state('visit.er-during', {
      url: '/er/during',
      views:{
        'step':{
          templateUrl: 'templates/records/during.html',
        }
      },
      back:'visit.er-induction',
    })

    .state('visit.er-sign-out', {
      url: '/er/sign-out',
      views:{
        'step':{
          templateUrl: 'templates/records/visit-sign-out.html',
        }
      },
    })



/****         ASSIST HSR     *****/



    .state('visit.hsr-assistance', {
      url: '/hsr/assistance',
      views:{
        'step':{
          templateUrl: 'templates/hsr/assistance.html',
        }
      },
      back:'visit.why',
    })

    .state('visit.hsr-sign-in', {
      url: '/hsr/sign-in',
      views:{
        'step':{
          templateUrl: 'templates/hsr/sign-in.html',
        }
      },
      back:'visit.hsr-assistance',
    })


    .state('visit.hsr-induction', {
      url: '/hsr/induction',
      views:{
        'step':{
          templateUrl: 'templates/hsr/visit-induction.html',
        }
      },
      back:'visit.hsr-sign-in',
      //fwd:'visit.both-current'
    })



    .state('visit.hsr-assist', {
      url: '/hsr/assist',
      views:{
        'step':{
          templateUrl: 'templates/hsr/assist.html',
        }
      },
      back:'visit.hsr-induction',
    })

    .state('visit.hsr-sign-out', {
      url: '/hsr/sign-out',
      views:{
        'step':{
          templateUrl: 'templates/hsr/visit-sign-out.html',
        }
      },
    })




/****      GENERAL SAFETY DISCUSSIONS    *******/


    .state('visit.gs-both-permits', {
      url: '/gs/permits',
      views:{
        'step':{
          templateUrl: 'templates/general-safety/visit-both-permits.html',
        }
      },

      back:'visit.why'
    })

    // .state('visit.gs-both-current', {
    //   url: '/gs/current',
    //   views:{
    //     'step':{
    //       templateUrl: 'templates/general-safety/visit-both-current.html',
    //     }
    //   },

    //   back:'visit.gs-both-permits'
    // })

    .state('visit.gs-24hrs-notice', {
      url: '/gs/24hrs-notice',
      views:{
        'step':{
          templateUrl: 'templates/general-safety/visit-24hrs-notice.html',
        }
      },
      back:'visit.gs-both-permits'
    })




    .state('visit.gs-sign-in', {
      url: '/gs/sign-in',
      views:{
        'step':{
          templateUrl: 'templates/general-safety/visit-sign-in.html',
        }
      },
      back:'visit.gs-24hrs-notice'
    })



    .state('visit.gs-induction', {
      url: '/gs/induction',
      views:{
        'step':{
          templateUrl: 'templates/general-safety/visit-induction.html',
        }
      },
      back:'visit.gs-sign-in',
      //fwd:'visit.both-current'
    })




    .state('visit.gs-during-investigation', {
      url: '/gs/during-investigation',
      views:{
        'step':{
          templateUrl: 'templates/general-safety/visit-during-investigation.html',
        }
      },
      back:'visit.gs-induction'
    })

    .state('visit.gs-sign-out', {
      url: '/gs/sign-out',
      views:{
        'step':{
          templateUrl: 'templates/general-safety/visit-sign-out.html',
        }
      },
    })



/****      Fair Work BREACH    *******/


    .state('visit.fwb-fw-permit', {
      url: '/fwb/fw-permit',
      views:{
        'step':{
          templateUrl: 'templates/fair-work/visit-fw-permit.html',
        }
      },

      back:'visit.why'
    })



    // .state('visit.fwb-fw-current', {
    //   url: '/fwb/fw-current',
    //   views:{
    //     'step':{
    //       templateUrl: 'templates/fair-work/visit-fw-current.html',
    //     }
    //   },

    //   back:'visit.fwb-fw-permit'
    // })


    .state('visit.fwb-24hrs-notice', {
      url: '/fwb/24hrs-notice',
      views:{
        'step':{
          templateUrl: 'templates/fair-work/visit-24hrs-notice.html',
        }
      },

      back:'visit.fwb-fw-permit'
    })

    .state('visit.fwb-sign-in', {
      url: '/fwb/sign-in',
      views:{
        'step':{
          templateUrl: 'templates/fair-work/visit-sign-in.html',
        }
      },

      back:'visit.fwb-24hrs-notice'
    })


    .state('visit.fwb-induction', {
      url: '/fwb/induction',
      views:{
        'step':{
          templateUrl: 'templates/fair-work/visit-induction.html',
        }
      },
      back:'visit.fwb-sign-in',
      //fwd:'visit.both-current'
    })



    .state('visit.fwb-during-investigation', {
      url: '/fwb/during-investigation',
      views:{
        'step':{
          templateUrl: 'templates/fair-work/visit-during-investigation.html',
        }
      },

      back:'visit.fwb-induction'
    })

    .state('visit.fwb-sign-out', {
      url: '/fwb/sign-out',
      views:{
        'step':{
          templateUrl: 'templates/fair-work/visit-sign-out.html',
        }
      },
    })


/****      Fair Work Genereal    *******/


    .state('visit.fwg-fw-permit', {
      url: '/fwg/fw-permit',
      views:{
        'step':{
          templateUrl: 'templates/general-fair-work/visit-fw-permit.html',
        }
      },

      back:'visit.why'
    })

    // .state('visit.fwg-fw-current', {
    //   url: '/fwg/fw-current',
    //   views:{
    //     'step':{
    //       templateUrl: 'templates/general-fair-work/visit-fw-current.html',
    //     }
    //   },

    //   back:'visit.fwg-fw-permit'
    // })

    .state('visit.fwg-24hrs-notice', {
      url: '/fwg/24hrs-notice',
      views:{
        'step':{
          templateUrl: 'templates/general-fair-work/visit-24hrs-notice.html',
        }
      },
      back:'visit.fwg-fw-permit'
    })

    .state('visit.breaks', {
      url: '/fwg/breaks',
      views:{
        'step':{
          templateUrl: 'templates/general-fair-work/visit-breaks.html',
        }
      },
      back:'visit.fwg-24hrs-notice'
    })

    .state('visit.fwg-sign-in', {
      url: '/fwg/sign-in',
      views:{
        'step':{
          templateUrl: 'templates/general-fair-work/visit-sign-in.html',
        }
      },

      back:'visit.breaks'
    })



    .state('visit.fwg-induction', {
      url: '/fwg/induction',
      views:{
        'step':{
          templateUrl: 'templates/general-fair-work/visit-induction.html',
        }
      },
      back:'visit.fwb-sign-in',
      //fwd:'visit.both-current'
    })



    .state('visit.fwg-during-investigation', {
      url: '/fwg/during-investigation',
      views:{
        'step':{
          templateUrl: 'templates/general-fair-work/visit-during-investigation.html',
        }
      },

      back:'visit.fwg-induction'
    })


    .state('visit.fwg-sign-out', {
      url: '/fwg/sign-out',
      views:{
        'step':{
          templateUrl: 'templates/general-fair-work/visit-sign-out.html',
        }
      },
    })


/******     *********/


/****      Fair Work Summaries    *******/


// .state('visit-summary-sb', {
//   url: '/visits/:id/summary',
//   templateUrl: 'templates/summaries/safety-breach.html',
//   controller: 'VisitCtrl'
// })



// .state('visit-summary-sg', {
//   url: '/visits/:id/summary',
//   templateUrl: 'templates/summaries/safety-breach.html',
//   controller: 'VisitCtrl'
// })



// .state('visit-summary-ib', {
//   url: '/visits/:id/summary',
//   templateUrl: 'templates/summaries/safety-breach.html',
//   controller: 'VisitCtrl'
// })


// .state('visit-summary-ig', {
//   url: '/visits/:id/summary',
//   templateUrl: 'templates/summaries/safety-breach.html',
//   controller: 'VisitCtrl'
// })




    ;


  $urlRouterProvider.otherwise('/visits');

 //     $compileProvider.imgSrcSanitizationWhitelist(/^\s(https|file|blob|cdvfile):|data:image\//);


  console.log('module.configure');
});
